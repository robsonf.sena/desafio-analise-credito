package dock.desafio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dock.desafio.model.Proposta;
import dock.desafio.repository.PropostasRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")

public class PropostaController {
	
	@Autowired
	PropostasRepository propostaRepository;
	
	/*@GetMapping("/propostas")
	public ResponseEntity<List<Proposta>> findAll() {
		List<Proposta> propostas = new ArrayList<Proposta>();
		propostaRepository.findAll().forEach(propostas::add);
		
		return new ResponseEntity<>(propostas, HttpStatus.OK);
	}*/
	
	@GetMapping("/propostas")
	public ResponseEntity<List<Proposta>> getAllPropostas(@RequestParam(required = false) String titulo) {
		try {
			
			List<Proposta> propostas = new ArrayList<Proposta>();
			
			if (titulo == null) {
				propostaRepository.findAll().forEach(propostas::add);
			} else {
				propostaRepository.findByTituloContaining(titulo).forEach(propostas::add);
			}
			
			if (propostas.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(propostas, HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping("/propostas/{id}")
	public ResponseEntity<Proposta> getPropostaById(@PathVariable("id") long id) {
		Optional<Proposta> PropostaData = propostaRepository.findById(id);
		
		if (PropostaData.isPresent()) {
			return new ResponseEntity<>(PropostaData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/propostas")
	public ResponseEntity<Proposta> createProposta(@RequestBody Proposta proposta) {
		try {
			Proposta _proposta = propostaRepository.save(new Proposta(proposta.getTitulo(), false));
			return new ResponseEntity<>(_proposta, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/proposta/{id}")
	public ResponseEntity<Proposta> updateProposta(@PathVariable("id") long id, @RequestBody Proposta proposta) {
		Optional<Proposta> propostaData = propostaRepository.findById(id);
		
		if (propostaData.isPresent()) {
			Proposta _proposta = propostaData.get();
			_proposta.setTitulo(proposta.getTitulo());
			_proposta.setAprovado(proposta.isAprovado());
			return new ResponseEntity<>(propostaRepository.save(_proposta), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("proposta/{id}")
	public ResponseEntity<HttpStatus> deleteProposta(@PathVariable("id") long id) {
		try {
			propostaRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/propostas/aprovados")
	public ResponseEntity<List<Proposta>> findByAprovado() {
		try {
			List<Proposta> propostas = propostaRepository.findByAprovado(false);
			
			if(propostas.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(propostas, HttpStatus.OK);	
			
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
