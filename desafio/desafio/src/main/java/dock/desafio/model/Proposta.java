package dock.desafio.model;

import javax.persistence.*;


@Entity
@Table(name = "Propostas")
public class Proposta {
	
	public Proposta() {
		
	}
	

	public Proposta(String titulo, boolean aprovado) {
		this.titulo = titulo;
		this.aprovado = aprovado;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "titulo")
	private String titulo;
	
	@Column(name = "aprovado")
	private boolean aprovado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isAprovado() {
		return aprovado;
	}

	public void setAprovado(boolean aprovado) {
		this.aprovado = aprovado;
	}
	
	@Override 
	public String toString() {
		return "Proposta id=" + id + ", Titulo=" + titulo + ", aprovado=" + aprovado + "";
	}
	
}
