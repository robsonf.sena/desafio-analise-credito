package dock.desafio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import dock.desafio.model.Proposta;

public interface PropostasRepository extends JpaRepository<Proposta, Long> {
	List<Proposta> findByTituloContaining(String titulo);
	List<Proposta> findByAprovado(boolean aprovado);	
}
